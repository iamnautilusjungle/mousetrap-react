import * as Mousetrap from "mousetrap";
import { useEffect, useRef } from "react";

const useMousetrap = (keys: string | string[], callback: (e: Mousetrap.ExtendedKeyboardEvent, combo: string) => any, action?: "keypress" | "keydown" | "keyup" | undefined) => {
    const cbRef = useRef<((e: Mousetrap.ExtendedKeyboardEvent, combo: string) => any) | null>(null);
    cbRef.current = callback;

    useEffect(() => {
        Mousetrap.bind(keys, (e: Mousetrap.ExtendedKeyboardEvent, combo: string) => {
            if (cbRef.current) {
                return cbRef.current(e, combo)
            }
        }, action)
        return () => {
            Mousetrap.unbind(keys, action);
        }
    }, [keys])
}

export default useMousetrap;